#!/usr/bin/env python3
"""
    Copyright (C) ilias iliadis, 2019; ilias iliadis <iliadis@kekbay.gr>

    This is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    It is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this file.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import cairo
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class MainWindow(Gtk.Window):
    def __init__(self, thepngpath):
        Gtk.Window.__init__(self, title="Example")

        self.resize_ratio = 1
        self.the_X = 0
        self.the_Y = 0

        #load the image into a permanent cairo ImageSurface
        self.theimage = cairo.ImageSurface.create_from_png(thepngpath)

        #window start size
        self.set_default_size(200, 200)

        #add a grid
        self.theGrid = Gtk.Grid()
        self.add(self.theGrid)

        #add a drawing area
        self.theDrawingArea = Gtk.DrawingArea()
        self.theGrid.attach(self.theDrawingArea,0,0,1,1)

        self.theDrawingArea.connect('configure-event', self.on_theDrawingArea_configure_event)
        self.theDrawingArea.connect('draw', self.on_theDrawingArea_draw)
        self.theDrawingArea.set_visible(True)
        self.theDrawingArea.set_hexpand(True)
        self.theDrawingArea.set_vexpand(True)

    def on_theDrawingArea_configure_event(self, widget, event, *args):
        """ Handler for theDrawingArea.configure-event. """
        self.get_rescaled_values()
        self.theDrawingArea.queue_draw()
        self.set_title( "Example: ratio= " + str(self.resize_ratio))
        return False

    def on_theDrawingArea_draw(self, widget, cr, *args):
        """ Handler for theDrawingArea.draw. """
        if self.theimage is not None:
            cr.save ()
            cr.translate(self.the_X, self.the_Y)
            cr.scale (self.resize_ratio, self.resize_ratio)
            cr.set_source_surface(self.theimage)
            cr.paint()
            cr.restore ()
        else:
            print('Invalid image')
        return False

    def get_rescaled_values(self):
        """ Recalculate the ratio and shift values. """
        img_W = self.theimage.get_width()
        img_H = self.theimage.get_height()
        al_W = self.theDrawingArea.get_allocated_width()
        al_H = self.theDrawingArea.get_allocated_height()
        resize_ratio = 1
        resize_ratio = al_W / img_W
        newimageH = img_H * resize_ratio
        if newimageH > al_H:
            resize_ratio = resize_ratio * (al_H / newimageH)

        self.the_X = (al_W - img_W * resize_ratio) / 2
        self.the_Y = (al_H - img_H * resize_ratio) / 2
        self.resize_ratio = resize_ratio

def main(APP_DIR):
    win = MainWindow(os.path.join(APP_DIR,"anything.png"))
    win.connect("destroy", lambda q: Gtk.main_quit())
    win.show_all()
    Gtk.main()

if __name__ == '__main__':
    #Main entry point if running the program from command line
    APP_DIR = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))
    main(APP_DIR)
